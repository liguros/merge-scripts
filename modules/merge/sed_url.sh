#!/bin/bash
#set -x

# Change to given directory
cd "${1}" || exit

function subs {
  sed -i -E -e "s/${1}/${2}/w tmpsed.txt" ${3}

  # If file tmpsed.txt exists and is not empty => we had a substitution
  if [ -s tmpsed.txt ]
  then
    echo "Substitution ${3} successful"
  else
    echo "Substitution ${3} unsuccessful"
  fi

  rm -f tmpsed.txt
}

function main_work {
# find all ebuilds under this directory and loop through the result
shopt -s globstar

for i in **/*.ebuild; do # Whitespace-safe and recursive
  # app-arch/afio - Update homepage and source URL
  if [[ $i =~ ^(.)*afio.(.)*ebuild$ ]];
  then
    builds["afio"]="1"
    subs "HOMEPAGE=\"https:\/\/(.)*" "HOMEPAGE=\"https:\/\/github.com\/kholtman\/afio\"" ${i}
    subs "SRC_URI=\"http:\/\/(.)*" "SRC_URI=\"https:\/\/github.com\/kholtman\/\$\{PN\}\/archive\/v\$\{PV\}.tar.gz\"" ${i}
  fi

  # sci-mathematics/4ti2 - fixing HOMEPAGE (SRC_URI will change with 1.6.9 release)
  if [[ $i =~ ^(.)*4ti2.(.)*ebuild$ ]];
  then
    builds["4ti2"]="1"
    subs "HOMEPAGE=\"https:\/\/www.4ti2.de\/" "HOMEPAGE=\"https:\/\/4ti2.github.io\/" ${i}
  #  subs "SRC_URI=\"http:\/\/(.)*" "MY_PV=\$\{PV\/\\\.\/\_\}\nSRC_URI=\"https:\/\/github.com\/4ti2\/\$\{PN\}\/archive\/Release_\$\{MY_PV\}.tar.gz\"" ${i}
  fi

  # media-video/aegisub - switching HOMEPAGE back to http
  if [[ $i =~ ^(.)*media-video/aegisub.(.)*ebuild$ ]];
  then
    builds["aegisub"]="1"
    subs "HOMEPAGE=\"https:\/\/" "HOMEPAGE=\"http:\/\/" ${i}
  fi

  # x11-wm/aewm - fixing HOMEPAGE
  if [[ $i =~ ^(.)*x11-wm/aewm.(.)*ebuild$ ]];
  then
    builds["aewm"]="1"
    subs "HOMEPAGE=\"https:\/\/(.)*" "HOMEPAGE=\"http:\/\/freshmeat.sourceforge.net\/projects\/aewm\"" ${i}
  fi

  # media-sound/alac_decoder - fixing HOMEPAGE (project seems to be dead)
  if [[ $i =~ ^(.)*media-sound/alac_decoder.(.)*ebuild$ ]];
  then
    builds["alac_decoder"]="1"
    subs "HOMEPAGE=\"https:\/\/(.)*" "HOMEPAGE=\"\https:\/\/web.archive.org\/web\/20150427073148\/https:\/\/craz.net\/programs\/itunes\/alac.html\"" ${i}
  fi

  # media-gfx/album - switching HOMEPAGE back to http as https is giving errors
  if [[ $i =~ ^(.)*media-gfx/album.(.)*ebuild$ ]];
  then
    builds["album"]="1"
    subs "HOMEPAGE=\"https:\/\/" "HOMEPAGE=\"http:\/\/" ${i}
  fi

  # media-video/alevt - fixing HOMEPAGE
  if [[ $i =~ ^(.)*media-video/alevt.(.)*ebuild$ ]];
  then
    builds["alevt"]="1"
    subs "HOMEPAGE=\"https:\/\/www.goron.de\/\~froese" "HOMEPAGE=\"https:\/\/gitlab.com\/alevt\/alevt" ${i}
  fi

  # media-libs/aalib - fixing HOMEPAGE
  if [[ $i =~ ^(.)*media-libs/aalib.(.)*ebuild$ ]];
  then
    builds["aalib"]="1"
    subs "HOMEPAGE=\"https:\/\/sourceforge.net\/projects\/aa-project\/aalib\/" "HOMEPAGE=\"http:\/\/aa-project.sourceforge.net\/aalib\/" ${i}
  fi

  # sci-electronics/alliance - fixing HOMEPAGE
  if [[ $i =~ ^(.)*sci-electronics/alliance.(.)*ebuild$ ]];
  then
    builds["alliance"]="1"
    subs "HOMEPAGE=\"https:\/\/soc-extras.lip6.fr\/en\/alliance-abstract-en\/" "HOMEPAGE=\"http:\/\/coriolis.lip6.fr\/" ${i}
  fi

  # mail-client/alpine - switching HOMEPAGE back to http as https is giving errors
  if [[ $i =~ ^(.)*mail-client/alpine.(.)*ebuild$ ]];
  then
    builds["alpine"]="1"
    subs "HOMEPAGE=\"https:\/\/" "HOMEPAGE=\"http:\/\/" ${i}
  fi

  # app-backup/amanda - switching HOMEPAGE back to http as https is giving errors
  if [[ $i =~ ^(.)*app-backup/amanda.(.)*ebuild$ ]];
  then
    builds["amanda"]="1"
    subs "HOMEPAGE=\"https:\/\/" "HOMEPAGE=\"http:\/\/" ${i}
  fi

  # net-analyzer/amap - fixing HOMEPAGE
  if [[ $i =~ ^(.)*net-analyzer/amap.(.)*ebuild$ ]];
  then
    builds["amap"]="1"
    subs "HOMEPAGE=\"https:\/\/www.thc.org\/thc-amap\/" "HOMEPAGE=\"https:\/\/github.com\/vanhauser-thc\/THC-Archive\/tree\/master\/Tools" ${i}
  fi

  # media-libs/ampache_browser - switching HOMEPAGE back to http as https is giving errors
  if [[ $i =~ ^(.)*media-libs/ampache_browser.(.)*ebuild$ ]];
  then
    builds["ampache_browser"]="1"
    subs "HOMEPAGE=\"https:\/\/" "HOMEPAGE=\"http:\/\/" ${i}
  fi

  # app-text/ansifilter - switching HOMEPAGE back to http as https is giving errors
  if [[ $i =~ ^(.)*app-text/ansifilter.(.)*ebuild$ ]];
  then
    builds["ansifilter"]="1"
    subs "HOMEPAGE=\"https:\/\/" "HOMEPAGE=\"http:\/\/" ${i}
  fi

  # app-text/antiword - switching HOMEPAGE back to http as https is giving errors
  if [[ $i =~ ^(.)*app-text/antiword.(.)*ebuild$ ]];
  then
    builds["antiword"]="1"
    subs "HOMEPAGE=\"https:\/\/" "HOMEPAGE=\"http:\/\/" ${i}
  fi

  # sys-power/apcupsd - switching HOMEPAGE back to http as https is giving errors
  if [[ $i =~ ^(.)*sys-power/apcupsd.(.)*ebuild$ ]];
  then
    builds["apcupsd"]="1"
    subs "HOMEPAGE=\"https:\/\/" "HOMEPAGE=\"http:\/\/" ${i}
  fi

  # app-admin/apg - fixing HOMEPAGE
  if [[ $i =~ ^(.)*app-admin/apg.(.)*ebuild$ ]];
  then
    builds["apg"]="1"
    subs "HOMEPAGE=\"https:\/\/www.adel.nursat.kz\/apg" "HOMEPAGE=\"https:\/\/web.archive.org\/web\/20130313042424\/http:\/\/www.adel.nursat.kz:80\/apg" ${i}
  fi

  # www-apache/mod_musicindex - switching HOMEPAGE back to http as https is giving errors
  if [[ $i =~ ^(.)*www-apache/mod_musicindex.(.)*ebuild$ ]];
  then
    builds["musicindex"]="1"
    subs "HOMEPAGE=\"https:\/\/" "HOMEPAGE=\"http:\/\/" ${i}
  fi

  # dev-java/appframework - fixing HOMEPAGE
  if [[ $i =~ ^(.)*dev-java/appframework.(.)*ebuild$ ]];
  then
    builds["appframework"]="1"
    subs "HOMEPAGE=\"https:\/\/java.net\/projects\/appframework" "HOMEPAGE=\"https:\/\/dev.gentoo.org\/~monsieurp\/packages\/" ${i}
  fi

  # dev-java/apt-mirror - fixing HOMEPAGE
  if [[ $i =~ ^(.)*dev-java/apt-mirror.(.)*ebuild$ ]];
  then
    builds["apt-mirror"]="1"
    subs "HOMEPAGE=\"https:\/\/aptmirrorapi.dev.java.net" "HOMEPAGE=\"https:\/\/docs.oracle.com\/javase\/7\/docs\/jdk\/api\/apt\/mirror\/overview-summary.html" ${i}
  fi

  # sci-mathematics/arb - fixing HOMEPAGE
  if [[ $i =~ ^(.)*sci-mathematics/arb.(.)*ebuild$ ]];
  then
    builds["arb"]="1"
    subs "HOMEPAGE=\"https:\/\/fredrikj.net\/arb\/" "HOMEPAGE=\"http:\/\/arblib.org\/" ${i}
  fi

  # dev-util/archdiff - fixing HOMEPAGE
  if [[ $i =~ ^(.)*dev-util/archdiff.(.)*ebuild$ ]];
  then
    builds["archivediff"]="1"
    subs "HOMEPAGE=\"https:\/\/frigidcode.com\/code\/archdiff\/" "HOMEPAGE=\"https:\/\/web.archive.org\/web\/20120511012558\/http:\/\/frigidcode.com:80\/code\/archdiff\/" ${i}
  fi

  # net-analyzer/arping - switching HOMEPAGE back to http as https is giving errors
  if [[ $i =~ ^(.)*net-analyzer/arping.(.)*ebuild$ ]];
  then
    builds["arping"]="1"
    subs "HOMEPAGE=\"https:\/\/" "HOMEPAGE=\"http:\/\/" ${i}
  fi

  # media-gfx/aaphoto - switching HOMEPAGE back to http as https is giving errors
  if [[ $i =~ ^(.)*media-gfx/aaphoto.(.)*ebuild$ ]];
  then
    builds["aaphoto"]="1"
    subs "HOMEPAGE=\"https:\/\/" "HOMEPAGE=\"http:\/\/" ${i}
  fi

  # dev-util/abi-compliance-checker - fixing HOMEPAGE
  if [[ $i =~ ^(.)*dev-util/abi-compliance-checker.(.)*ebuild$ ]];
  then
    builds["abi-compliance"]="1"
    subs "HOMEPAGE=\"https:\/\/ispras.linuxbase.org\/index.php\/ABI_compliance_checker" "HOMEPAGE=\"https:\/\/github.com\/lvc\/abi-compliance-checker" ${i}
  fi

  # x11-misc/accessx - fixing HOMEPAGE
  if [[ $i =~ ^(.)*x11-misc/accessx.(.)*ebuild$ ]];
  then
    builds["accessx"]="1"
    subs "HOMEPAGE=\"https:\/\/cita.disability.uiuc.edu\/software\/accessx\/freewareaccessx.php" "HOMEPAGE=\"https:\/\/web.archive.org\/web\/20150801144644\/http:\/\/cita.disability.uiuc.edu\/software\/accessx\/freewareaccessx.php" ${i}
  fi

  # app-benchmarks/acovea - fixing HOMEPAGE
  if [[ $i =~ ^(.)*app-benchmarks/acovea.(.)*ebuild$ ]];
  then
    builds["acovea"]="1"
    subs "HOMEPAGE=\"https:\/\/www.coyotegulch.com\/products\/acovea\/" "HOMEPAGE=\"https:\/\/github.com\/Acovea\/libacovea" ${i}
  fi

  # app-misc/actkbd - fixing HOMEPAGE
  if [[ $i =~ ^(.)*app-misc/actkbd.(.)*ebuild$ ]];
  then
    builds["actkbd"]="1"
    subs "HOMEPAGE=\"https:\/\/users.softlab.ece.ntua.gr\/~thkala\/projects\/actkbd\/" "HOMEPAGE=\"https:\/\/github.com\/thkala\/actkbd" ${i}
  fi

  # games-emulation/advancescan - fixing HOMEPAGE
  if [[ $i =~ ^(.)*games-emulation/advancescan.(.)*ebuild$ ]];
  then
    builds["advancescan"]="1"
    subs "HOMEPAGE=\"https:\/\/sourceforge.net\/projects\/advancemame\/scan-readme.html" "HOMEPAGE=\"http:\/\/www.advancemame.it\/scan-readme.html" ${i}
  fi

  # net-misc/aget - switching HOMEPAGE back to http as https is giving errors
  if [[ $i =~ ^(.)*net-misc/aget.(.)*ebuild$ ]];
  then
    builds["aget"]="1"
    subs "HOMEPAGE=\"https:\/\/" "HOMEPAGE=\"http:\/\/" ${i}
  fi

  # net-wireless/airtraf - switching HOMEPAGE back to http as https is giving errors
  if [[ $i =~ ^(.)*net-wireless/airtraf.(.)*ebuild$ ]];
  then
    builds["airtraf"]="1"
    subs "HOMEPAGE=\"https:\/\/" "HOMEPAGE=\"http:\/\/" ${i}
  fi

  # x11-plugins/allin1 - fixing HOMEPAGE
  if [[ $i =~ ^(.)*x11-plugins/allin1.(.)*ebuild$ ]];
  then
    builds["allin1"]="1"
    subs "HOMEPAGE=\"https:\/\/ilpettegolo.altervista.org\/linux_allin1.en.shtml" "HOMEPAGE=\"https:\/\/www.dockapps.net\/allin1" ${i}
  fi

  # media-sound/alsaplayer - switching HOMEPAGE back to http as https is giving errors
  if [[ $i =~ ^(.)*media-sound/alsaplayer.(.)*ebuild$ ]];
  then
    builds["alsaplayer"]="1"
    subs "HOMEPAGE=\"https:\/\/" "HOMEPAGE=\"http:\/\/" ${i}
  fi

  # www-apps/Apache-Gallery - fixing HOMEPAGE
  if [[ $i =~ ^(.)*www-apps/Apache-Gallery.(.)*ebuild$ ]];
  then
    builds["apachegallery"]="1"
    subs "HOMEPAGE=\"https:\/\/apachegallery.dk\/" "HOMEPAGE=\"https:\/\/metacpan.org\/pod\/Apache::Gallery" ${i}
  fi

  # sci-biology/amap - fixing HOMEPAGE
  if [[ $i =~ ^(.)*sci-biology/amap.(.)*ebuild$ ]];
  then
    builds["bioamap"]="1"
    subs "HOMEPAGE=\"https:\/\/bio.math.berkeley.edu\/amap\/" "HOMEPAGE=\"https:\/\/web.archive.org\/web\/20060902044446\/http:\/\/bio.math.berkeley.edu\/amap\/" ${i}
  fi

  # app-editors/amyedit - switching HOMEPAGE back to http as https is giving errors
  if [[ $i =~ ^(.)*app-editors/amyedit.(.)*ebuild$ ]];
  then
    builds["amyedit"]="1"
    subs "HOMEPAGE=\"https:\/\/" "HOMEPAGE=\"http:\/\/" ${i}
  fi

  # dev-java/ant-owanttask - fixing HOMEPAGE
  if [[ $i =~ ^(.)*dev-java/ant-owanttask.(.)*ebuild$ ]];
  then
    builds["owant"]="1"
    subs "HOMEPAGE=\"https:\/\/monolog.objectweb.org" "HOMEPAGE=\"https:\/\/web.archive.org\/web\/20070814042614\/http:\/\/monolog.objectweb.org\/" ${i}
  fi

  # www-apache/mod_auth_tkt - switching HOMEPAGE back to http as https is giving errors
  if [[ $i =~ ^(.)*www-apache/mod_auth_tkt.(.)*ebuild$ ]];
  then
    builds["authtkt"]="1"
    subs "HOMEPAGE=\"https:\/\/" "HOMEPAGE=\"http:\/\/" ${i}
  fi

  # www-apache/mod_auth_xradius - fixing HOMEPAGE
  if [[ $i =~ ^(.)*www-apache/mod_auth_xradius.(.)*ebuild$ ]];
  then
    builds["authxradius"]="1"
    subs "HOMEPAGE=\"https:\/\/www.outoforder.cc\/projects\/apache\/mod_auth_xradius\/" "HOMEPAGE=\"http:\/\/www.outoforder.cc\/projects\/httpd\/mod_auth_xradius\/" ${i}
  fi

  # www-apache/mod_bw - fixing HOMEPAGE
  if [[ $i =~ ^(.)*www-apache/mod_bw.(.)*ebuild$ ]];
  then
    builds["mod_bw"]="1"
    subs "HOMEPAGE=\"https:\/\/www.ivn.cl\/apache\/" "HOMEPAGE=\"http:\/\/wp.ivn.cl\/apache-bandwidth-mod\/" ${i}
  fi

  # www-apache/mod_extract_forwarded - fixing HOMEPAGE
  if [[ $i =~ ^(.)*www-apache/mod_extract_forwarded.(.)*ebuild$ ]];
  then
    builds["extract_forwarded"]="1"
    subs "HOMEPAGE=\"https:\/\/www.openinfo.co.uk\/apache\/index.html" "HOMEPAGE=\"http:\/\/www.warhound.org\/mod_extract_forwarded\/" ${i}
  fi

  # www-apache/mod_log_sql - fixing HOMEPAGE
  if [[ $i =~ ^(.)*www-apache/mod_log_sql.(.)*ebuild$ ]];
  then
    builds["mod_log_sql"]="1"
    subs "HOMEPAGE=\"https:\/\/www.outoforder.cc\/projects\/apache\/mod_log_sql\/" "HOMEPAGE=\"http:\/\/www.outoforder.cc\/projects\/httpd\/mod_log_sql\/" ${i}
  fi

  # www-apache/mod_rpaf - fixing HOMEPAGE
  if [[ $i =~ ^(.)*www-apache/mod_rpaf.(.)*ebuild$ ]];
  then
    builds["mod_rpaf"]="1"
    subs "HOMEPAGE=\"https:\/\/stderr.net\/apache\/rpaf\/" "HOMEPAGE=\"https:\/\/github.com\/gnif\/mod_rpaf" ${i}
  fi

  # www-apache/mod_vdbh - fixing HOMEPAGE
  if [[ $i =~ ^(.)*www-apache/mod_vdbh.(.)*ebuild$ ]];
  then
    builds["mod_vdbh"]="1"
    subs "HOMEPAGE=\"https:\/\/www.synthemesc.com\/mod_vdbh\/" "HOMEPAGE=\"https:\/\/web.archive.org\/web\/20050219190056\/www.synthemesc.com\/mod_vdbh\/" ${i}
  fi

  # dev-util/appdata-tools - fixing HOMEPAGE
  if [[ $i =~ ^(.)*dev-util/appdata-tools.(.)*ebuild$ ]];
  then
    builds["appdata-tools"]="1"
    subs "HOMEPAGE=\"https:\/\/github.com\/hughsie\/appdata-tools\/" "HOMEPAGE=\"https:\/\/github.com\/jmatsuzawa\/appdata-website" ${i}
  fi

  # dev-util/appinventor - switching HOMEPAGE back to http as https is giving errors
  if [[ $i =~ ^(.)*dev-util/appinventor.(.)*ebuild$ ]];
  then
    builds["appinventor"]="1"
    subs "HOMEPAGE=\"https:\/\/" "HOMEPAGE=\"http:\/\/" ${i}
  fi

  # sys-apps/apply-default-acl - switching HOMEPAGE back to http as https is giving errors
  if [[ $i =~ ^(.)*sys-apps/apply-default-acl.(.)*ebuild$ ]];
  then
    builds["apply_default"]="1"
    subs "HOMEPAGE=\"https:\/\/" "HOMEPAGE=\"http:\/\/" ${i}
  fi

  # x11-misc/apwal - switching HOMEPAGE back to http as https is giving errors
  if [[ $i =~ ^(.)*x11-misc/apwal.(.)*ebuild$ ]];
  then
    builds["apwal"]="1"
    subs "HOMEPAGE=\"https:\/\/" "HOMEPAGE=\"http:\/\/" ${i}
  fi

  # sci-chemistry/aqua - fixing HOMEPAGE
  if [[ $i =~ ^(.)*sci-chemistry/aqua.(.)*ebuild$ ]];
  then
    builds["aqua"]="1"
    subs "HOMEPAGE=\"https:\/\/www.biochem.ucl.ac.uk\/~roman\/procheck\/procheck.html" "HOMEPAGE=\"https:\/\/www.ebi.ac.uk\/thornton-srv\/software\/PROCHECK\/" ${i}
  fi

  # media-fonts/aquafont - fixing HOMEPAGE
  if [[ $i =~ ^(.)*media-fonts/aquafont.(.)*ebuild$ ]];
  then
    builds["aquafont"]="1"
    subs "HOMEPAGE=\"https:\/\/www.geocities.jp\/teardrops_in_aquablue\/" "HOMEPAGE=\"http:\/\/aquafont.wouau.com\/Teardrops%20in%20Aquablue.htm" ${i}
  fi
done
}

# declare associative array for check if ebuild still exists
# Usage:
# define a value for each ebuild condition you check in main_work above and set it to "0"
# if the "if"-condition is true set the associated value to "1"
# at the end of the script all values which are still not set to "1" display a warning
# that the if condition was not triggered
declare -A builds=(
  ["afio"]="0"
  ["4ti2"]="0"
  ["aegisub"]="0"
  ["aewm"]="0"
  ["alac_decoder"]="0"
  ["album"]="0"
  ["alevt"]="0"
  ["aalib"]="0"
  ["alliance"]="0"
  ["alpine"]="0"
  ["amanda"]="0"
  ["amap"]="0"
  ["ampache_browser"]="0"
  ["ansifilter"]="0"
  ["antiword"]="0"
  ["apcupsd"]="0"
  ["apg"]="0"
  ["musicindex"]="0"
  ["appframework"]="0"
  ["apt-mirror"]="0"
  ["arb"]="0"
  ["archivediff"]="0"
  ["arping"]="0"
  ["aaphoto"]="0"
  ["abi-compliance"]="0"
  ["accessx"]="0"
  ["acovea"]="0"
  ["actkbd"]="0"
  ["advancescan"]="0"
  ["aget"]="0"
  ["airtraf"]="0"
  ["allin1"]="0"
  ["alsaplayer"]="0"
  ["apachegallery"]="0"
  ["bioamap"]="0"
  ["amyedit"]="0"
  ["owant"]="0"
  ["authtkt"]="0"
  ["authxradius"]="0"
  ["mod_bw"]="0"
  ["extract_forwarded"]="0"
  ["mod_log_sql"]="0"
  ["mod_rpaf"]="0"
  ["mod_vdbh"]="0"
  ["appdata-tools"]="0"
  ["appinventor"]="0"
  ["apply_default"]="0"
  ["apwal"]="0"
  ["aqua"]="0"
  ["aquafont"]="0"
  )

main_work
echo

# display warning for all conditions that were not triggered
for i in "${!builds[@]}"; do
  if [[ "${builds[${i}]}" != "1" ]];
  then
    echo "Ebuilds for ${i} not found. Please check."
  fi
done
