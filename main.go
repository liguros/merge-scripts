package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"

	"gitlab.com/bluebottle/go-modules/misc"
	"gitlab.com/liguros/merge-scripts/configuration"

	git "github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/transport/ssh"
	"github.com/otiai10/copy"
)

// Return public ssh key
func publicKey() *ssh.PublicKeys {
	sshPath := os.Getenv("HOME") + "/.ssh/id_rsa"
	sshKey, err := os.ReadFile(sshPath)
	misc.ShowError(err, "", "ErrPanic")
	publicKey, err := ssh.NewPublicKeys("git", []byte(sshKey), "")
	misc.ShowError(err, "", "ErrPanic")
	return publicKey
} // publicKey ()

// Clone the desired repo with given name and URL. If needed a ssh key will be used.
func cloneRepo(name, url string, reference plumbing.ReferenceName, sshNeeded bool) (*git.Repository, error) {
	var (
		err     error
		tmpRepo *git.Repository
	)

	if sshNeeded {
		tmpRepo, err = git.PlainClone(name, false, &git.CloneOptions{
			URL:           url,
			ReferenceName: "refs/heads/" + reference,
			Progress:      os.Stdout,
			Auth:          publicKey(),
		})
	} else {
		tmpRepo, err = git.PlainClone(name, false, &git.CloneOptions{
			URL:           url,
			ReferenceName: "refs/heads/" + reference,
			Progress:      os.Stdout,
		})
	} // if

	return tmpRepo, err
} // cloneRepo ()

// Execute shell command and display output
func cmdOutput(cmd string, args ...string) ([]byte, error) {
	b, err := exec.Command(cmd, args...).CombinedOutput()
	return b, err
} // cmdOutput ()

// Handle eclasses, filters and copying, from given repository to liguros-repo
func foundationWork(reponame string, v *configuration.Definition) {
	// Copy eclasses to liguros/eclass
	for _, val := range v.Eclasses {
		fmt.Println("Copying eclass: " + reponame + "/eclass/" + val + ".eclass to liguros-repo/eclass/" + val + ".eclass")
		err := copy.Copy(reponame+"/eclass/"+val+".eclass", "liguros-repo/eclass/"+val+".eclass")
		misc.ShowError(err, "", "ErrPanic")
	} // for

	var tmpDirect string

	// Copy files to liguros
	for _, val := range v.Copyfiles {
		tmpDirect = val.Destination

		if val.Destination == "" {
			tmpDirect = val.Source
		} // if

		fmt.Println("Copying file: " + reponame + "/" + val.Source + " to liguros-repo/" + tmpDirect)
		err := copy.Copy(reponame+"/"+val.Source, "liguros-repo/"+tmpDirect)
		misc.ShowError(err, "", "ErrPanic")
	} // for

	// Filter packages, eclasses or ebuilds
	if len(v.Filter) > 0 {
		for _, val := range v.Filter {
			files, err := filepath.Glob(reponame + "/" + val)
			misc.ShowError(err, "", "ErrPanic")

			if len(files) > 0 {
				for _, file := range files {
					fmt.Println("Removing: " + file)
					err = os.RemoveAll(file)
					misc.ShowError(err, "", "ErrPanic")
				} // for
			} else {
				fmt.Println("NOTICE Filter: " + reponame + "/" + val + " not found")
			} // if
		} // for
	} // if
} // foundationWork ()

// Copy package over to liguros, with removing existing package at destination first
//
// TODO: Check if this can be replaced with copy.Copy?
func copyPackage(pack string) {
	fmt.Println("Package: " + pack)

	// Check if source directory exists
	if _, err := os.Stat(pack); !os.IsNotExist(err) {
		// Check if directory exist in liguros, if yes => remove it
		if _, err := os.Stat("../liguros-repo/" + pack); !os.IsNotExist(err) {
			fmt.Println("Removing: ../liguros-repo/" + pack)
			err = os.RemoveAll("../liguros-repo/" + pack)
			misc.ShowError(err, "", "ErrPanic")
		} // if

		fmt.Println("Copying: " + pack + " to ../liguros-repo/" + pack)
		err = copy.Copy(pack, "../liguros-repo/"+pack)
		misc.ShowError(err, "", "ErrPanic")
	} else {
		fmt.Println("NOTICE Package " + pack + " not existing")
	} // if
} // copyPackage ()

// Copy category over to liguros with removing existing category at destination first
//
// TODO: Check if this can be replaced with copy.Copy?
func copyCategory(cat string) {
	fmt.Println("Category: " + cat)
	packs, err := filepath.Glob(cat + "/*")
	misc.ShowError(err, "", "ErrPanic")

	for _, pack := range packs {
		mode, err := os.Stat(pack)
		misc.ShowError(err, "", "ErrPanic")

		if mode.Mode().IsDir() {
			copyPackage(pack)
		} // if
	} // for
} // copyCategory ()

// Copy repository to liguros (skipping metadata, profiles and .git)
func copyRepo(repo string) {
	fmt.Println("Copy all from " + repo + " to liguros")
	err := os.Chdir(repo)
	misc.ShowError(err, "", "ErrPanic")
	cats, err := filepath.Glob("*")
	misc.ShowError(err, "", "ErrPanic")

	for _, cat := range cats {
		mode, err := os.Stat(cat)
		misc.ShowError(err, "", "ErrPanic")

		if mode.Mode().IsDir() && cat != "metadata" && cat != "profiles" && cat != ".git" {
			copyCategory(cat)
		} // if
	} // for

	err = os.Chdir("..")
	misc.ShowError(err, "", "ErrPanic")
} // copyRepo

func main() {
	// Read config file.
	configValues := configuration.ReadProgConfig("foundation.toml", ".")

	// Clone Liguros repo first
	fmt.Println("\nCloning liguros-repo")
	ligurosRepo, err := cloneRepo("liguros-repo", configValues.Liguros.URL, configValues.Liguros.Reference, true)
	misc.ShowError(err, "", "ErrPanic")
	wt, err := ligurosRepo.Worktree()
	misc.ShowError(err, "", "ErrPanic")

	// Remove all from liguros repo
	fmt.Println("\nRemove all from liguros-repo")
	_, err = wt.Remove(".")
	misc.ShowError(err, "", "ErrPanic")

	// Clone gentoo staging
	fmt.Println("\nCloning gentoo staging")
	_, err = cloneRepo("gentoo-staging", configValues.GentooStage.URL, configValues.GentooStage.Reference, false)
	misc.ShowError(err, "", "ErrPanic")

	// Clean up gentoo repo of unwanted stuff
	fmt.Println("\nClean gentoo staging from unwanted stuff")
	foundationWork("gentoo-staging", &configValues.GentooStage)

	// Copy all the rest (except .git) of gentoo repo into liguros repo (as base system)
	fmt.Println("\nMoving gentoo staging to liguros repo as base")
	files, err := os.ReadDir("gentoo-staging")
	misc.ShowError(err, "", "ErrPanic")

	for _, file := range files {
		if file.Name() != ".git" {
			fmt.Println("Copying gentoo-staging/" + file.Name() + " to liguros-repo/" + file.Name())
			errRes := copy.Copy("gentoo-staging/"+file.Name(), "liguros-repo/"+file.Name())
			misc.ShowError(errRes, "", "ErrPanic")
		} // if
	} // for

	// Loop through all the defined repos and add them to the liguros repo
	for i, v := range configValues.Repo {
		fmt.Println("\nCloning repository: ", i)
		_, err = cloneRepo(i, v.URL, v.Reference, false)
		misc.ShowError(err, "", "ErrPanic")
		// Do the basic work with the foundation config
		foundationWork(i, &v)

		// Copy selected packages to liguros if set, otherwise copy all packages left
		if len(v.Select) > 0 {
			fmt.Println("Copy selected files from " + i + " to liguros")
			err := os.Chdir(i)
			misc.ShowError(err, "", "ErrPanic")

			for _, w := range v.Select {
				fmt.Println("Copy: ", w)
				copyPackage(w)
			} // for

			err = os.Chdir("..")
			misc.ShowError(err, "", "ErrPanic")
		} else {
			copyRepo(i)
		} // if
	} // for

	// Clone ports
	fmt.Println("\nCloning ports")
	_, err = cloneRepo("ports", configValues.Ports.URL, configValues.Ports.Reference, false)
	misc.ShowError(err, "", "ErrPanic")
	foundationWork("ports", &configValues.Ports)
	copyRepo("ports")

	// Clone fixups
	fmt.Println("\nCloning fixups")
	_, err = cloneRepo("kit-fixups", configValues.Fixups.URL, configValues.Fixups.Reference, false)
	misc.ShowError(err, "", "ErrPanic")
	foundationWork("kit-fixups", &configValues.Fixups)
	copyRepo("kit-fixups")
	// copy profiles over from fixups to liguros repo
	fmt.Println("Copy kit-fixups/profiles to liguros-repo/profiles")
	err = copy.Copy("kit-fixups/profiles", "liguros-repo/profiles")
	misc.ShowError(err, "", "ErrPanic")

	// Run misc_sed command on the repo
	fmt.Println("Running misc_sed replacements on the repo")
	b, err := cmdOutput("sh", "-c", "bin/misc_sed.sh liguros-repo")
	misc.ShowError(err, string(b), "ErrMsgPanic")

	// Push liguros repo back to gitlab
	// fmt.Println("\nAdding altered files to work tree")
	// // go-git still has the problem of not adding deleted files to the tree => using workaround
	// cmd := exec.Command("git", "add", ".")
	// cmd.Dir = wt.Filesystem.Root()
	// err = cmd.Run()
	// misc.ErrorPanic(err)

	// fmt.Println("\nCommitting changes to liguros repo")
	// _, err = wt.Commit("Updating liguros repo", &git.CommitOptions{})
	// misc.ErrorPanic(err)

	fmt.Println("\nAdding back liguros repo to gitlab disabled in program. Changes will be pushed after metadata is generated.")
	// fmt.Println("\nPushing liguros repo to gitlab")
	// err = ligurosRepo.Push(&git.PushOptions{
	// 	Progress: os.Stdout,
	// 	Auth:     publicKey(),
	// })
	// misc.ErrorPanic(err)
} // main ()
