# Go parameters
GOCMD      := go
GOBUILD    := GO111MODULE=on $(GOCMD) build -v -mod=vendor
GOCLEAN    := $(GOCMD) clean
GOTEST     := GO111MODULE=on $(GOCMD) test -v
GOGET      := GO111MODULE=on $(GOCMD) get -v
GOVET      := GO111MODULE=on $(GOCMD) vet
GORACE     := $(GOCMD) race
GOTOOL     := GO111MODULE=on $(GOCMD) tool
GOLINT     := golint

SRCS       := $(shell find . -name "*.go" -type f ! -path "./vendor/*" ! -path "*/bindata.go")
VERSION    := $(shell git describe --always --long --dirty 2>/dev/null)

ifeq ($(VERSION),)
	VERSION := master
endif

MDATE      := $(shell date -u +%Y%m%d.%H%M%S)
LDFLAGS    := $(LDFLAGS) -X "main.Builddate=$(MDATE)"
DEVLDFL    := $(LDFLAGS) -X "main.Version=$(VERSION)-dev"
EXECUTABLE := merge-scripts

export GO111MODULE=off

PACKAGES ?= $(shell GO111MODULE=on $(GOCMD) list -mod=vendor ./... | grep -v /vendor/)

ifndef VERBOSE
.SILENT:
endif

.DEFAULT_GOAL := help

all: dep build coverage  ## Run all for development

dep: ## Get all the dependencies
		@echo "Getting dependencies"
		$(GOGET) -d ./...
		#$(GOCMD) get -u github.com/fzipp/gocyclo/cmd/gocyclo
		#$(GOCMD) get -u github.com/uudashr/gocognit/cmd/gocognit
		#$(GOCMD) get -u github.com/gordonklaus/ineffassign
		#$(GOCMD) get -u github.com/kisielk/errcheck
		#$(GOCMD) get -v github.com/securego/gosec

fmt: ## Format source code
		@echo "Formatting code"
		GO111MODULE=on $(GOCMD) fmt $(PACKAGES)

test: ## Running tests for files
		@echo "Running tests"
		@echo "============="
		#@echo "Running gocyclo"
		#gocyclo -over 15 -ignore vendor .
		#@echo "Running gocognit"
		#gocognit -over 15 .|grep -v vendor
		#@echo "Running ineffassign"
		#ineffassign .
#		@echo "Running misspell"
#		misspell * | grep -v vendor
#		# Run error check
		#@echo "Running errcheck"
		#GO111MODULE=on errcheck -asserts -blank -verbose ./...
		#@echo "Running gosec"
		#GO111MODULE=on gosec -no-fail -tests ./...
		@echo "Running $(GOVET)"
		$(GOVET) ./.
		@echo "Running $(GOTEST) race condition"
		$(GOTEST) -race ./.
		@echo "Running $(GOTEST) race memory sanity"
		CC=clang $(GOTEST) -msan ./.
		@echo "Running tests"
		$(GOTEST) ./. || exit 1;
		@echo "Running benchs"
		$(GOTEST) -bench ./.
		@echo ""

build: ## Building development binary
		@echo "Running $(GOBUILD)"
		@echo "================"
		$(GOBUILD) -tags development -ldflags '$(DEVLDFL) $(LDFLAGS)' -o $(EXECUTABLE) ./. ;
		$(GOBUILD) -tags development -ldflags '$(DEVLDFL) $(LDFLAGS)' -o bin/metadata/metadata bin/metadata/*.go ;
		@echo ""

coverage: ## Generating coverage for files
		@echo "Running coverage"
		@echo "================"
		$(GOTEST) ./... -coverpkg=./... -coverprofile coverage.cov
		$(GOTOOL) cover -func=coverage.cov

clean: ## Cleaning up
		@echo "Cleaning package"
		@echo "================"
		rm -f ${EXECUTABLE} bin/metadata/metadata
		rm -rf binaryden cinnamon farmboy0 fusion809 gentoo-staging go-overlay kit-fixups librewolf liguros-repo mate-desktop pf4public ports steam stefantalpalaru thegreatmcpain python_compats*

help:  ## Displaying help for build targets
		@echo "Available targets in this makefile:"
		@echo ""
		@grep -E '^[ a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | \
		awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}'
