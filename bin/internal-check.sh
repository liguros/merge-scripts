#set -x

# Update each given local repo with its remote counterpart
# Parameters
# $1 = repo name
# $2 = local repo
# $3 = remote repo
function update_repo {
  echo "Updating $1"
  echo "Cloning/updating local repo $2"

  if [ -d $1 ];
    then
      cd $1; git pull; cd ..;
    else
      git clone $2 $1
  fi

  # find all ebuilds under this directory and loop through the result
  echo "Adding origin info ($1) to metadata files"
  cd $1

  if [ $? -eq 0 ];
    then
      set -e
      /tmp/metadata -r $1
      set +e

      # Update repo
      echo "Pushing local repo $1"
      git add .
      git commit -m "$1 updates"
      git push
      echo ""
      cd ..
  else
    exit 1;
  fi
}

# update ports
update_repo ports git@gitlab.com:liguros/ports.git
# update kit-fixups
update_repo kit-fixups git@gitlab.com:liguros/kit-fixups.git
