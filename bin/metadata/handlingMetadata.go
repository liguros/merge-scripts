package main

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"io"
	"os"
	"strings"

	"gitlab.com/bluebottle/go-modules/misc"
)

// PkgMetaData describes the main structure of the metadata file
type PkgMetaData struct {
	XMLName      xml.Name          `xml:"pkgmetadata"`
	Maintainers  []Maintainer      `xml:"maintainer"`
	LongDescript []LongDescription `xml:"longdescription"`
	Upstreams    Upstream          `xml:"upstream,omitempty"`
	Uses         []Use             `xml:"use"`
	Origin       string            `xml:"origin,omitempty"`
	Slots        []Slot            `xml:"slots"`
	// Use pointer to check if tag was given but is empty
	StabilizeAllarches *string `xml:"stabilize-allarches"`
}

// Slot describes slot structure
type Slot struct {
	Subslots string `xml:"subslots,omitempty"`
	Name     string `xml:"lang,attr,omitempty"`
}

// LongDescription describes the long description structure
type LongDescription struct {
	XMLName xml.Name `xml:"longdescription"`
	Name    string   `xml:"lang,attr,omitempty"`
	Value   string   `xml:",innerxml"`
}

// Maintainer describes the maintainer structure
type Maintainer struct {
	XMLName      xml.Name      `xml:"maintainer"`
	Type         string        `xml:"type,attr,omitempty"`
	Status       string        `xml:"status,attr,omitempty"`
	Email        string        `xml:"email,omitempty"`
	Name         string        `xml:"name,omitempty"`
	Descriptions []Description `xml:"description,omitempty"`
}

// Description describes the description structure
type Description struct {
	XMLName xml.Name `xml:"description"`
	Name    string   `xml:"lang,attr,omitempty"`
	Value   string   `xml:",chardata"`
}

// Upstream describes the upstream structure
type Upstream struct {
	RemoteIDs   []RemoteID   `xml:"remote-id,omitempty"`
	Maintainers []Maintainer `xml:"maintainer"`
	Changelog   string       `xml:"changelog,omitempty"`
	BugsTo      string       `xml:"bugs-to,omitempty"`
	Docs        []Doc        `xml:"doc,omitempty"`
}

// Doc describes the doc structure
type Doc struct {
	XMLName xml.Name `xml:"doc"`
	Lang    string   `xml:"lang,attr,omitempty"`
	Value   string   `xml:",chardata"`
}

// RemoteID describes the remote-id structure
type RemoteID struct {
	XMLName xml.Name `xml:"remote-id"`
	Type    string   `xml:"type,attr"`
	Value   string   `xml:",chardata"`
}

// Use describes the use structure
type Use struct {
	Flags []Flag `xml:"flag,omitempty"`
	Lang  string `xml:"lang,attr,omitempty"`
}

// Flag describes the flag structure
type Flag struct {
	XMLName xml.Name `xml:"flag"`
	Name    string   `xml:"name,attr"`
	Value   string   `xml:",innerxml"`
}

// identReader returns input reader to avoid unmarshal error
func identReader(encoding string, input io.Reader) (io.Reader, error) {
	return input, nil
}

// reads given metadata file and updates content
func handlingMetadata(filename, repo string) {
	fmt.Println("Updating file: " + filename)
	xmlFile, err := os.Open(filename)
	misc.ShowError(err, "", "ErrFatal")
	// Read in file and decode it into structure
	b, err := io.ReadAll(xmlFile)
	misc.ShowError(err, "", "ErrFatal")
	var q PkgMetaData
	d := xml.NewDecoder(bytes.NewReader(b))
	d.CharsetReader = identReader

	if err := d.Decode(&q); err != nil {
		misc.ShowError(err, "NOTICE: Decoding error", "ErrMsgPrint")
	} // if

	misc.ShowError(err, ". NOTICE: Error decoding "+filename, "ErrMsgPrint")
	err = xmlFile.Close()
	misc.ShowError(err, "", "ErrFatal")

	// update origin if it is not set or different from given repo
	if q.Origin == "" || q.Origin != repo {
		q.Origin = repo
	} //if

	// Add cpe line if it is needed and not present
	cpeFound, cpeValue := checkForCPE(q)
	value, ok := cpeSubst[strings.TrimSuffix(filename, "/metadata.xml")]

	if !cpeFound && ok {
		// there is no cpe line in the metadata file but we have a value in the map
		q.Upstreams.RemoteIDs = append(q.Upstreams.RemoteIDs, RemoteID{Type: "cpe", Value: "cpe:/a:" + value})
	} else if cpeFound && ok {
		// there is a cpe line in the metadata file and we have a value in the map
		if cpeValue != ("cpe:/a:" + value) {
			fmt.Println("NOTICE: CPE for " + value + " different than in file. Replacing it")

			var tmpRemote []RemoteID

			for i := 0; i < len(q.Upstreams.RemoteIDs); i++ {
				if q.Upstreams.RemoteIDs[i].Type == "cpe" {
					// Remove remote ID entry if given value is TODELETE, otherwise append the changed value
					if value != "TODELETE" {
						tmpRemote = append(tmpRemote,
							RemoteID{XMLName: q.Upstreams.RemoteIDs[i].XMLName, Type: q.Upstreams.RemoteIDs[i].Type, Value: "cpe:/a:" + value})
					} // if
				} else {
					tmpRemote = append(tmpRemote, q.Upstreams.RemoteIDs[i])
				} // if
			} // for

			q.Upstreams.RemoteIDs = tmpRemote
		} else {
			fmt.Println("NOTICE: CPE " + value + " already in metafile. Please remove from map.")
		} // if
	} // if

	tmpMaintain := []Maintainer{}

	for _, v := range q.Maintainers {
		// Filter some maintainers
		switch v.Email {
		case "matthew@gentoo.org":
			// Ignore those
		default:
			tmpMaintain = append(tmpMaintain, v)
		} // switch
	} // for

	tmpRemote := []RemoteID{}

	for _, v := range q.Upstreams.RemoteIDs {
		// remove google-code, cpan, pear, sourceforge, bitbucket, gitlab, github and heptapod remote ids, as they are quite often badly maintained,
		// no longer existing and sometimes the URL resolution is wrong (e.g. repology)
		switch v.Type {
		case "google-code", "cpan", "cpan-module", "pear", "pypi", "sourceforge", "bitbucket", "gitlab", "github", "heptapod", "osdn", "gentoo", "sourcehut", "freedesktop-gitlab", "savannah-nongnu", "savannah", "gnome-gitlab", "hackage", "kde-invent", "codeberg", "vim":
			// Ignore those
		default:
			tmpRemote = append(tmpRemote, v)
		} // switch
	} // for

	// write back the resulting ids
	q.Upstreams.RemoteIDs = tmpRemote
	q.Maintainers = tmpMaintain
	// write updated content back
	file, err := xml.MarshalIndent(q, "", " ")
	misc.ShowError(err, "", "ErrFatal")
	// Fix stabilize to self closing tag
	file = bytes.ReplaceAll(file, []byte("<stabilize-allarches></stabilize-allarches>"), []byte("<stabilize-allarches/>"))
	// Fix empty upstream tag
	file = bytes.ReplaceAll(file, []byte("<upstream></upstream>"), []byte(""))
	// Update xml header with doctype
	const header = `<?xml version="1.0" encoding="UTF-8"?>` + "\n" + `<!DOCTYPE pkgmetadata SYSTEM "https://liguros.gitlab.io/dtd/metadata.dtd">` + "\n"
	file = []byte(header + string(file))
	err = os.WriteFile(filename, file, 0644)
	misc.ShowError(err, "Error writing file: ", "ErrMsgFatal")
} // handlingMetadata ()

// Check if the metadata file contains a cpe line or not
func checkForCPE(q PkgMetaData) (cpeFound bool, cpeValue string) {
	cpeFound = false

	for _, v := range q.Upstreams.RemoteIDs {
		if v.Type == "cpe" {
			cpeFound = true
			cpeValue = v.Value
		} // if
	} // for

	return
} // checkForCPE ()
