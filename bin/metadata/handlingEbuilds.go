package main

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"gitlab.com/bluebottle/go-modules/misc"
)

// reads given ebuild and change some values
func handlingEbuilds(filename, repo string) {
	// check if we need to change something in the ebuild
	dir, _ := filepath.Split(filename)
	value, ok := sedEbuildSubst[strings.TrimSuffix(dir, "/")]

	if ok && value.repo == repo {
		inFile, err := os.Open(filename)
		misc.ShowError(err, "", "ErrFatal")
		// Read in file
		b, err := io.ReadAll(inFile)
		misc.ShowError(err, "", "ErrFatal")
		err = inFile.Close()
		misc.ShowError(err, "", "ErrFatal")
		fmt.Println("Updating file: " + filename)

		for i, v := range value.subst {
			r, err := regexp.Compile(v.src)
			misc.ShowError(err, "", "ErrFatal")

			// Check if we find the regex. If not print a notice.
			if r.Match(b) {
				// Only replace strings literally, when they contain no backslash
				if strings.Contains(v.dest, `\`) {
					b = r.ReplaceAll(b, []byte(v.dest))
				} else {
					b = r.ReplaceAllLiteral(b, []byte(v.dest))
				} // if
			} else {
				// Show which regex was not found in ebuild
				fmt.Printf("Notice: Regex %d not found in ebuild\n", i)
			} // if
		} // for

		// write updated content back
		err = os.WriteFile(filename, b, 0644)
		misc.ShowError(err, "Error writing file: ", "ErrMsgFatal")
		// Mark ebuild as changed
		value.Changed = true
	} // if
} // handlingEbuilds ()
