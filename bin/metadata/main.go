package main

import (
	"io"
	"fmt"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"github.com/urfave/cli/v2"
	"gitlab.com/bluebottle/go-modules/misc"
)

func CopyFile(src, dst string) (int64, error) {
  sf, err := os.Open(src)
 
  if err != nil {
    return 0, err
  } // if

  defer sf.Close()
  df, err := os.Create(dst)

  if err != nil {
    return 0, err
  } // if

  defer df.Close()
  return io.Copy(df, sf)
} // CopyFile()


// Loop through directories to verify that each package has a meta file. If not, create a dummy file. Then proceed to the normal meta file handling.
// Also substitute multiple strings in ebuilds
func looping(repo string) {
	var paths, ebuilds = make(map[string]bool), make(map[string]bool)

	// Walk through all ebuilds and add the directory to a map
	err := filepath.Walk(".", func(path string, info os.FileInfo, err error) error {
		if strings.HasSuffix(path, ".ebuild") {
			paths[filepath.Dir(path)] = true
			ebuilds[path] = true
		} // if

		return nil
	})

	fmt.Println("Updating metadata files")

	// Loop through map and handle the metadata files
	for k := range paths {
		tmpName := k + "/metadata.xml"

		// Check if metadata file does not exist and create it
		if _, err := os.Stat(tmpName); err != nil {
			fmt.Println("NOTICE " + tmpName + " does not exist - creating tmp one")
			CopyFile("/tmp/metadata.template", tmpName)
		} // if

		handlingMetadata(tmpName, repo)
	} // for

	misc.ShowError(err, "", "ErrFatal")
	fmt.Printf("Updating ebuilds for repo %s\n", repo)
	// Build sorted list of ebuilds
	keys := make([]string, 0, len(ebuilds))

	for k := range ebuilds {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	// Loop through ebuilds
	for _, v := range keys {
		handlingEbuilds(v, repo)
	} // for

	// Loop through all ebuilds and print those which have not been found and should belong in the current repo
	fmt.Printf("Following ebuilds have not been found in repo %s. Please check.\n", repo)

	for k, v := range sedEbuildSubst {
		if !v.Changed && v.repo == repo {
			fmt.Println("Notice: " + k)
		} // if
	} // for
} // looping

func main() {
	var repo string
	app := &cli.App{
		Name:                   "metadata",
		Usage:                  "Update metadata files (use metadata -h for usage)",
		UseShortOptionHandling: true,
		Version:                "0.2.3",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "repo",
				Aliases:     []string{"r"},
				Usage:       "Name of the repo the metadata comes from",
				Destination: &repo,
			},
		},
		Action: func(c *cli.Context) error {
			if repo != "" {
				looping(repo)
			} else {
				fmt.Println(c.App.Usage)
			} // if

			return nil
		},
	}

	fmt.Println(app.Name + "v" + app.Version)
	err := app.Run(os.Args)
	misc.ShowError(err, "", "ErrFatal")
} // main ()
