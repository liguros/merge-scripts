# shellcheck shell=bash
#set -x

# integrate glsa into gentoo-staging
function gentoo_glsa {
  echo "Integrate glsa into gentoo-staging"
  cd ..

  # Clone glsa (glsa)
  if [ -d "glsa" ];
    then
      cd glsa || return
      git pull
      cd .. || return
    else
      git clone git://anongit.gentoo.org/data/glsa.git
  fi

  cd gentoo-staging || return
  # Copy glsa over
  mkdir metadata/glsa || return
  cp ../glsa/* metadata/glsa
  # Remove some stuff
  rm -f metadata/.gitignore
  find . -name 'glsa-200*.xml' -type f -delete
  find . -name 'glsa-201[0-8]*.xml' -type f -delete
}

# Update each given local repo with its remote counterpart
# Parameters
# $1 = repo name
# $2 = local repo
# $3 = remote repo
function update_repo {
  echo "Updating $1"
  echo "Cloning/updating local repo $2"

  if [ -d "$1" ];
    then
      cd "$1" || return
      git pull || exit 1
      cd .. || return
    else
      git clone --depth 2 "$2" "$1" || exit 1
  fi

  echo "Cloning/updating remote repo $3"

  if [ -d "$1-remote" ];
    then
      cd "$1-remote" || return
      git pull || exit 1
      cd .. || return
    else
      git clone --depth 2 "$3" "$1-remote" || exit 1
  fi

  echo "Update local content with remote content"
  cd "$1" || return
  rm -rf ./*
  cp -rf ../"$1-remote"/* .

  # Mix in glsa for gentoo-staging
  if [ "$1" = "gentoo-staging" ];
    then
      gentoo_glsa
  fi

  # find all ebuilds under this directory and loop through the result
  echo "Adding origin info ($1) to metadata files"
  set -x
  /tmp/metadata -r "$1"
  set +x

  # Update repo
  echo "Pushing local repo $1"
  git add .
  git commit -m "$1 updates"
  git push
  cd ..
  echo ""
}

# update gentoo-staging
update_repo gentoo-staging git@gitlab.com:liguros/gentoo-staging.git https://anongit.gentoo.org/git/repo/gentoo.git
# update fusion809 overlay
update_repo fusion809-overlay git@gitlab.com:liguros/fusion809-overlay.git https://github.com/fusion809/fusion809-overlay.git
# update go overlay
update_repo go-overlay git@gitlab.com:liguros/go-overlay.git https://github.com/Dr-Terrible/go-overlay.git
# update stefantalpalaru overlay
update_repo stefantalpalaru-overlay git@gitlab.com:liguros/stefantalpalaru-overlay.git https://github.com/stefantalpalaru/gentoo-overlay.git
# update steam overlay
update_repo steam-overlay git@gitlab.com:liguros/steam-overlay.git https://github.com/anyc/steam-overlay
# update mate overlay
update_repo mate-overlay git@gitlab.com:liguros/mate-de-gentoo.git https://github.com/gentoo-mirror/mate-de-gentoo.git
# update thegreatmcpain overlay
update_repo thegreatmcpain-overlay git@gitlab.com:liguros/TheGreatMcPain-overlay.git https://github.com/TheGreatMcPain/TheGreatMcPain-overlay.git
# update librewolf overlay
#update_repo librewolf-overlay git@gitlab.com:liguros/librewolf.git https://gitlab.com/librewolf-community/browser/gentoo.git
update_repo librewolf-overlay git@gitlab.com:liguros/librewolf.git https://codeberg.org/librewolf/gentoo.git
# update pf4public overlay
update_repo pf4public-overlay git@gitlab.com:liguros/pf4-overlay.git https://github.com/PF4Public/gentoo-overlay.git
# update binaryden overlay
update_repo binaryden-overlay git@gitlab.com:liguros/overlays/binaryden.git https://github.com/tkemmer/binaryden.git
# update gentoo guru overlay
update_repo gentoo-guru-overlay git@gitlab.com:liguros/overlays/gentoo-guru.git https://github.com/gentoo-mirror/guru.git
# update slonko overlay
update_repo slonko-overlay git@gitlab.com:liguros/overlays/slonko.git https://github.com/gentoo-mirror/slonko.git
