# shellcheck shell=bash
# Runs various sed/awk inline fixes to the ebuilds/tree.
# set -x
# Change to given directory
cd "${1}" || exit

# find all ebuilds under this directory and loop through the result
shopt -s globstar

if [[ -e ${CI_PROJECT_DIR}/python_compats.txt ]]; then
  rm "${CI_PROJECT_DIR}"/python_compats.txt
fi
if [[ -e ${CI_PROJECT_DIR}/python_compats_fixed.txt ]]; then
  rm "${CI_PROJECT_DIR}"/python_compats_fixed.txt
fi

# set correct repo name
sed -i -E 's/kit-fixups/liguros-repo/g' "metadata/layout.conf"

for i in **/*.ebuild; do # Whitespace-safe and recursive
  # Grab the state of PYTHON_COMPAT in liguros-repo before making changes
  grep PYTHON_COMPAT= "${i}" >> "${CI_PROJECT_DIR}"/python_compats.txt
  sort -u "${CI_PROJECT_DIR}"/python_compats.txt > "${CI_PROJECT_DIR}"/python_compats-sorted.txt
  # Various fixes to PYTHON_COMPAT
  # Some packages need fixed PYTHON_COMPAT settings thus we don't change them
  if [[ ! $i =~ ^(.)*dev-python/pypy3-exe.(.)*ebuild$ && \
        ! $i =~ ^(.)*dev-python/pypy-exe.(.)*ebuild$ && \
	! $i =~ ^(.)*dev-python/pypy3.(.)*ebuild$ && \
	! $i =~ ^(.)*dev-qt/qtwebengine.(.)*ebuild$ && \
	! $i =~ ^(.)*sys-apps/kmod.(.)*ebuild$ && \
	! $i =~ ^(.)*dev-python/pyside2.(.)*ebuild && \
	! $i =~ ^(.)*dev-python/wxpython.(.)*ebuild && \
	! $i =~ ^(.)*dev-python/d2d1.(.)*ebuild && \
	! $i =~ ^(.)*dev-python/autocommand.(.)*ebuild && \
	! $i =~ ^(.)*www-servers/uwsgi.(.)*ebuild && \
	! $i =~ ^(.)*app-pda/libimobiledevice.(.)*ebuild && \
	! $i =~ ^(.)*app-emulation/virtualbox.(.)*ebuild && \
	! $i =~ ^(.)*www-client/waterfox-g.(.)*ebuild && \
	! $i =~ ^(.)*dev-qt/qtwebengine.(.)*ebuild && \
	! $i =~ ^(.)*media-libs/opencv.(.)*ebuild && \
	! $i =~ ^(.)*media-libs/aubio.(.)*ebuild && \
	! $i =~ ^(.)*dev-libs/libdnet.(.)*ebuild && \
	! $i =~ ^(.)*net-wireless/lorcon.(.)*ebuild && \
	! $i =~ ^(.)*net-wireless/aircrack-ng.(.)*ebuild && \
	! $i =~ ^(.)*dev-libs/Ice.(.)*ebuild && \
	! $i =~ ^(.)*dev-php/swoole.(.)*ebuild && \
	! $i =~ ^(.)*app-shells/bash-completion.(.)*ebuild && \
	! $i =~ ^(.)*mail-client/thunderbird.(.)*ebuild && \
	! $i =~ ^(.)*dev-lang/python-exec.(.)*ebuild && \
	! $i =~ ^(.)*dev-lang/spidermonkey.(.)*ebuild && \
	! $i =~ ^(.)*dev-libs/libpwquality.(.)*ebuild ]];
  then
    sed -i -E -f /tmp/sed-python.txt "${i}"
  fi
  # Grab the state of PYTHON_COMPAT in liguros-repo after running sed script
  grep PYTHON_COMPAT= "${i}" >> "${CI_PROJECT_DIR}"/python_compats_fixed.txt
  sort -u "${CI_PROJECT_DIR}"/python_compats_fixed.txt > "${CI_PROJECT_DIR}"/python_compats_fixed-sorted.txt

  # fixing LUA_COMPAT but not for dev-libs/efl and mail-filter/rspamd
  if [[ ! $i =~ ^(.)*dev-libs/efl.(.)*ebuild$ && ! $i =~ ^(.)*mail-filter/rspamd.(.)*ebuild$ ]];
  then
    sed -i -E 's/^LUA_COMPAT.*/LUA_COMPAT=( lua5-{1,3,4} luajit )/g' "${i}"
  fi

  # fixing epatch for dev-libs/libgamin
  if [[ $i =~ ^(.)*dev-libs/libgamin.(.)*ebuild$ ]];
  then
    sed -i -E 's/inherit((.)*) epatch ((.)*)/inherit \1 \3/g' "${i}"
    sed -i -E 's/epatch/eapply/g' "${i}"
  fi

  # fix for removal of virtual/pam switch to sys-libs/pam
  # this should catch any overlays that didn't switch deps yet
  sed -i -E -e 's/pam\? \( virtual\/pam \)/pam\? ( sys-libs\/pam )/g' "${i}"
  sed -i -e 's|dev-util/gtk-doc-am|dev-build/gtk-doc-am|' "${i}"
  sed -i -e 's|sys-devel/autoconf|dev-build/autoconf|' "${i}"
  sed -i -e 's|sys-devel/libtool|dev-build/libtool|' "${i}"

  # fix eutils, which is now deprecated
  sed -i -E -e 's/ eutils //g' "${i}"
  sed -i -E -e 's/^inherit eutils$//g' "${i}"

  # fix EAPI 6, which is no longer supported by most eclasses
  # remove eapi7-ver and versionator as well in the process
  sed -i -E -e 's/EAPI=6/EAPI=7/g' "${i}"
  sed -i -E -e 's/EAPI=\"6\"/EAPI=7/g' "${i}"
  sed -i -E -e 's/^((.)*)eapi7-ver((.)*)/\1\3/g' "${i}"
  sed -i -E -e 's/^((.)*)versionator((.)*)/\1\3/g' "${i}"

  # Fix switch from DISTUTILS_USE_SETUPTOOLS=pyproject.toml to DISTUTILS_USE_PEP517=setuptools
  sed -i -E -e 's/DISTUTILS_USE_SETUPTOOLS=pyproject.toml/DISTUTILS_USE_PEP517=setuptools/g' "${i}"

  # Fix app-misc/mc where USE=unicode can only be applied if USE=!slang
  if [[ $i =~ ^(.)*mc(.)*ebuild$ ]];
  then
    sed -i 's/^IUSE=\"+edit gpm nls samba sftp +slang spell test unicode X +xdg\"/IUSE=\"+edit gpm nls samba sftp slang spell test +unicode X +xdg\"/g' "${i}"
  fi

  # Fix for linux-headers depending on rsync - liguros/bugs#30:
  if [[ $i =~ ^(.)*linux-headers-[56789].(.)*ebuild$ ]];
  then
    sed -i -e 's|app-arch/xz-utils|app-arch/xz-utils\n\tnet-misc/rsync|' "${i}"
  fi
  
  # Fix for x11-wm/marco
  if [[ $i =~ ^(.)*x11-wm/marco.(.)*ebuild$ ]];
  then
    sed -i -e 's|sys-devel/libtool|dev-build/libtool|' "${i}"
  fi
  
  # Fix for libmateweather
  if [[ $i =~ ^(.)*dev-libs/libmateweather.(.)*ebuild$ ]];
  then
    sed -i -e 's|sys-devel/libtool|dev-build/libtool|' "${i}"
  fi
  
  # fixing wayland dependencies for muffin
  if [[ $i =~ ^(.)*muffin-6.(.)*ebuild$ ]];
  then
    sed -i -E 's|x11-base/xwayland|x11-base/xwayland\n\tgui-libs/egl-wayland\n\tdev-libs/libinput|g' "${i}"
    sed -i -E 's|^COMDEPEND|REQUIRED_USE="wayland? ( udev )"\nCOMDEPEND|g' "${i}"
  fi

  # Fix for lxde-common missing dep intltool
  if [[ $i =~ ^(.)*lxde-base/lxde-common.(.)*ebuild$ ]];
  then
    sed -i -e 's|sys-devel/gettext|sys-devel/gettext\n\t>=dev-util/intltool-0.40.0|' "${i}"
  fi

  # Fix dependency for debian-sources
  if [[ $i =~ ^(.)*sys-kernel/debian-sources.(.)*ebuild$ ]];
  then
    sed -i -e 's|libdrm\[libkms\]|libdrm|' "${i}"
  fi

  # Fix for cinnamon missing dep glib-utils and xorg-proto
  if [[ $i =~ ^(.)*gnome-extra/cinnamon.(.)*ebuild$ ]];
  then
    sed -i -e 's|x11-libs/libX11|x11-libs/libX11\n\tdev-util/glib-utils\n\tx11-base/xorg-proto|' "${i}"
  fi

  # Fix for texinfo missing dep help2man
  if [[ $i =~ ^(.)*sys-apps/texinfo.(.)*ebuild$ ]];
  then
    sed -i -e 's|RDEPEND=\"|RDEPEND=\"\n\tsys-apps/help2man|' "${i}"
  fi

  # Fix for harfbuzz missing dep - liguros/bugs#121:
  if [[ $i =~ ^(.)*harfbuzz.(.)*ebuild$ ]];
  then
    sed -i -e 's|dev-build/gtk-doc-am|dev-build/gtk-doc-am\n\tdev-util/gtk-doc|' "${i}"
  fi

  # Fix for librsvg missing dep - liguros/bugs#119:
  if [[ $i =~ ^(.)*librsvg.(.)*ebuild$ ]];
  then
    sed -i -e 's|^IUSE=\"gtk-doc|IUSE=\"+gtk-doc|' "${i}"
    sed -i -e 's|virtual/cargo|virtual/cargo\n\tdev-python/pygments|' "${i}"
  fi

  if [[ $i =~ ^(.)*babl.(.)*ebuild$ ]];
  then
    sed -i -e 's|virtual/pkgconfig|virtual/pkgconfig\n\tdev-libs/gobject-introspection|' "${i}"
  fi

  # Fix for net-misc/vino missing dep - liguros/bugs#142 and liguros/bugs#146:
  if [[ $i =~ ^(.)*vino.(.)*ebuild$ ]];
  then
    sed -i -e 's|gnome-base/gnome-common|gnome-base/gnome-common\n\tsys-devel/autoconf-archive|' "${i}"
  fi

  # Remove funtoo branding from gcc ebuilds - liguros/bugs#106:
  if [[ $i =~ ^(.)*toolchain.(.)*eclass$ ]];
  then
    sed -i -e 's|export\ BRANDING_GCC_PKGVERSION="Gentoo\ ${GCC_PVR}"|export BRANDING_GCC_PKGVERSION="LiGurOS ${GCC_PVR}"|' "${i}"
    sed -i -e 's|--with-bugurl=http:\/\/bugs\.gentoo\.org \\|--with-bugurl=https://bugs.liguros.org \\|' "${i}"
  fi

  # Adding fix for cinnamon needing gtk-doc for running automake and autoconf
  if [[ $i =~ ^(.)*cinnamon-4.(.)*ebuild$ ]];
  then
    sed -i -E -e 's|^(PATCHES=\()|BDEPEND="dev-util/gtk-doc"\n\1|' "${i}"
  fi

  # Fix for lxde-base/lxpanel missing dep - liguros/bugs#239:
  if [[ $i =~ ^(.)*lxpanel.(.)*ebuild$ ]];
  then
    sed -i -e 's|virtual/pkgconfig|virtual/pkgconfig\n\tdev-util/intltool|' "${i}"
  fi

  # Fix for mesa missing wayland-scanner dependency - liguros/bugs#256:
  if [[ $i =~ ^(.)*mesa-2[01].(.)*ebuild$ ]];
  then
    sed -i -e 's|\[\[ \-n \$platforms \]\] &&.*|emesonargs+=(-Dplatforms=${platforms#,})|' "${i}"
    sed -i -e 's|>=dev-libs/wayland-protocols-1.8|>=dev-libs/wayland-protocols-1.8\n\t\tdev-util/wayland-scanner|' "${i}"
  fi

  # Fix for colord-gtk missing app-text/docbook-xsl-ns-stylesheets dependency - liguros/bugs#260:
  # Fix for kwayland missing wayland-scanner dependency - liguros/bugs#268:
  if [[ $i =~ ^(.)*kwayland-5.80.(.)*ebuild$ ]];
  then
    sed -i -e 's|>=dev-libs/wayland-protocols-1.1.1|>=dev-libs/wayland-protocols-1.1.1\n\t\tdev-util/wayland-scanner|' "${i}"
  fi

  # Fix for gst-plugins-base missing wayland-scanner dependency:
  if [[ $i =~ ^(.)*media-libs/gst-plugins-base.(.)*ebuild$ ]];
  then
    sed -i -e 's|>=dev-libs/wayland-protocols-1.15|>=dev-libs/wayland-protocols-1.15\n\t\tdev-util/wayland-scanner|' "${i}"
  fi

  # Fix for kguiaddons missing wayland-scanner dependency:
  if [[ $i =~ ^(.)*kde-frameworks/kguiaddons.(.)*ebuild$ ]];
  then
    sed -i -e 's|dev-libs/wayland|dev-libs/wayland\n\t\tdev-util/wayland-scanner|' "${i}"
  fi

  # Fix for gtk missing wayland-scanner dependency:
  if [[ $i =~ ^(.)*gui-libs/gtk.(.)*ebuild$ ]];
  then
    sed -i -e 's|>=dev-libs/wayland-protocols-1.25|>=dev-libs/wayland-protocols-1.25\n\t\tdev-util/wayland-scanner|' "${i}"
    sed -i -e 's|>=dev-libs/wayland-protocols-1.31|>=dev-libs/wayland-protocols-1.31\n\t\tdev-util/wayland-scanner|' "${i}"
  fi

  # Fix for libmediaart missing glib-genmarshall dependency:
  if [[ $i =~ ^(.)*media-libs/libmediaart.(.)*ebuild$ ]];
  then
    sed -i -e 's|virtual/pkgconfig|virtual/pkgconfig\n\t\tdev-util/glib-utils|' "${i}"
  fi

  # Fix for gnutls missing brotli dependency:
  if [[ $i =~ ^(.)*net-libs/gnutls.(.)*ebuild$ ]];
  then
    sed -i -e 's|dev-build/gtk-doc-am|dev-build/gtk-doc-am\n\tapp-arch/brotli|' "${i}"
  fi

  # Fix for libkscreen missing Qt5LinguistTools dependency:
  if [[ $i =~ ^(.)*kde-plasma/libkscreen-5.(.)*ebuild$ ]];
  then
    sed -i -e 's|dev-libs/wayland|dev-libs/wayland\n\t>=dev-qt/linguist-tools-${QTMIN}:5|' "${i}"
  fi

  # Fix for karchive missing linguist-tools dependency:
  if [[ $i =~ ^(.)*kde-frameworks/karchive-5.(.)*ebuild$ ]];
  then
    sed -i -e 's|sys-libs/zlib|sys-libs/zlib\n\t\tdev-qt/linguist-tools|' "${i}"
  fi

  # Fix for gedit missing gspell dependency:
  if [[ $i =~ ^(.)*app-editors/gedit.(.)*ebuild$ ]];
  then
    sed -i -e 's|gnome-base/gvfs|gnome-base/gvfs\n\t\tapp-text/gspell|' "${i}"
  fi

  # Fix for libkscreen missing wayland-scanner dependency and plasma-wayland-protocols - liguros/bugs#294:
  if [[ $i =~ ^(.)*libkscreen-5.23.(.)*ebuild$ ]];
  then
    sed -i -e 's|x11-libs/libxcb|x11-libs/libxcb\n\tdev-util/wayland-scanner\n\tdev-libs/plasma-wayland-protocols|' "${i}"
  fi

  # Fix for colord-gtk missing app-text/docbook-xsl-ns-stylesheets dependency - liguros/bugs#260:
  if [[ $i =~ ^(.)*colord-gtk.(.)*ebuild$ ]];
  then
    sed -i -e 's|dev-libs/libxslt|dev-libs/libxslt\n\t\tapp-text/docbook-xsl-ns-stylesheets|' "${i}"
  fi

  # Fix ca-certificates dependency for cryptography - liguros/bugs#299:
  if [[ $i =~ ^(.)*ca-certificates-20211016.3.72.(.)*ebuild$ ]];
  then
    sed -i -e 's|dev-python/cryptography|<dev-python/cryptography-35|' "${i}"
  fi
  # Fix numpy cython dependency - liguros/bugs#320:
  if [[ $i =~ ^(.)*numpy-1.22.4.(.)*ebuild$ ]];
  then
    sed -i -e 's|>=dev-python/cython-0.29.24|>=dev-python/cython-0.29.30|' "${i}"
  fi

  # Fix for cinnamon dependency on pypam (masked) - liguros/bugs#322:
  if [[ $i =~ ^(.)*cinnamon-5.(.)*ebuild$ ]];
  then
    sed -i -e 's|dev-python/pypam|dev-python/python-pam|' "${i}"
  fi
done
