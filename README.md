Go program to merge gentoo like repositories into a final repository defined by a config file (foundation.toml).

Each repository in the config file can have a few parameters:

- copyfile: List of files to be copied (source and dest, if dest is not defined then dest is source)
- eclasses: List of eclasses to be copied
- filter: List of categories, packages or package versions to exclude from final repository
- select: List of packages to include in the final repository (if not given => all packages are used)
- URL: URL of the repository to include

filter also accepts wildcard, like:

- dev-util/bazel/bazel0*
- dev-util/bazel/bazel[0-1]*
